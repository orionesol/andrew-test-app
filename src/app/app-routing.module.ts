import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserDetailFormComponent } from './user-detail-form/user-detail-form.component';

const routes: Routes = [
  { path: 'userInfo/:id', component: UserDetailFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
