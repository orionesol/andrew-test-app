import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GetDetailComponent } from './get-detail/get-detail.component';
import { UserListComponent } from './user-list/user-list.component';
import {DataTableModule} from "angular-6-datatable";
import { UserDetailFormComponent } from './user-detail-form/user-detail-form.component';
@NgModule({
  declarations: [
    AppComponent,
    GetDetailComponent,
    UserListComponent,
    UserDetailFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    DataTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
