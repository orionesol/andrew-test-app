
/*  smart component that contains the grid and the user detail Value */

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";
@Component({
  selector: 'app-get-detail',
  templateUrl: './get-detail.component.html',
  styleUrls: ['./get-detail.component.css']
})
export class GetDetailComponent implements OnInit {
/*  Array Type  Observable */
  list :Observable<any[]>

  constructor(http: HttpClient) {
    const path ='https://jsonplaceholder.typicode.com/users'
    this.list =http.get<{items:any[]}>(path)
    .pipe(
      map((data:any) =>data))
    }

  ngOnInit() {
  }

}
