/* Dumb Component which Follow the instruction and get data from smart component */

import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class UserListComponent implements OnInit {
  /* FETCH user detail */
@Input() list :any[]

/* Observe Click Event And Get perticular Information of user According to user */
  userDetail:Observable<any[]>
  
  constructor() { }

  ngOnInit() {
  }
  getUserDetail(user:any){
    this.userDetail = user
}
}
