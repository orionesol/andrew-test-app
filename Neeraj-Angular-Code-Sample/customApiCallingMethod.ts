/* Custom Get ,Post, Put, Delete, Method With custom  promises and Header    */


import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }
  getOptions() {
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');
    headers.set("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
    headers.set('Access-Control-Allow-Origin', '*');
    headers.set('SkipLoader', 'yes');
    return { headers: headers, skip: true };
  }

  post(url: any, obj: any) {
    const promise = new Promise((resolve, reject) => {
      this.http.post(url, obj, this.getOptions())
        .toPromise()
        .then(
          responseJson => {
            resolve({ receiveObj: responseJson, success: true });
          }
        ).catch(error => {
          resolve({ receiveObj: error, success: false });
        });
    });

    return promise;
  }

  get(url: any) {
    const promise = new Promise((resolve, reject) => {
      this.http.get(url, this.getOptions())
        .toPromise()
        .then(
          responseJson => {
            resolve({ receiveObj: responseJson, success: true });
          }
        ).catch(error => {
          resolve({ receiveObj: error, success: false });
        });
    });

    return promise;
  }

  put(url: any, obj: any) {
    const promise = new Promise((resolve, reject) => {
      this.http.put(url, obj, this.getOptions())
        .toPromise()
        .then(
          responseJson => {
            resolve({ receiveObj: responseJson, success: true });
          }
        ).catch(error => {
          resolve({ receiveObj: error, success: false });
        });
    });

    return promise;
  }

  delete(url: any) {
    const promise = new Promise((resolve, reject) => {
      this.http.delete(url, this.getOptions())
        .toPromise()
        .then(
          responseJson => { // Success
            resolve({ receiveObj: responseJson, success: true });
          }
        ).catch(error => {
          resolve({ receiveObj: error, success: false });
        });
    });

    return promise;
  }
}


