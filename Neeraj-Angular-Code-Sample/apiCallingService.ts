import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpService } from './customApiCallingMethod';


declare let config: any;
@Injectable({
  providedIn: 'root'
})
export class CustomApiService {
    constructor(private http: HttpService, private httpservice: HttpClient) { }

    InsertConference(objModel: any) {
       return this.http.post(`${config.apiUrl}user/background`, objModel);
     
      }
}