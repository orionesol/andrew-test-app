import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LoginComponent } from './login/login.component';
import { MainsignupComponent } from './signup/mainsignup.component';
import { AuthGuard } from './_guards';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ConferenceListingComponent } from './conference/conference-listing/conference-listing.component';

import { OrgSignupComponent } from './organisation-login/org-signup/org-signup.component';
import { MembershipSetupComponent } from './membership/membership-setup/membership-setup.component';
import { MembershipSuccessComponent } from './membership/membership-success/membership-success.component';
import { ErrorPageComponent } from './error-page/error-page.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'landing-page', pathMatch: 'full'
  },

  { path: 'resetpassword/:id', component: ResetPasswordComponent },

  {
    "path": "conference-listing",
    "component": ConferenceListingComponent
  },

  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },


   /*        Auth Guard   */
  
  
   {
    path: "dashboards",
    component: DashboardComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "landing-page",
    component: LandingPageComponent
  },
  {
    path: 'organisation',
    loadChildren: './organisation-login/organisation-login.module#OrganisationLoginModule',
  },
  {
    "path": "organization-signup",
    "component": OrgSignupComponent
  },
  {
    path: 'sign-up', component: MainsignupComponent
  },
  {
    "path": "membership-setup/:id",
    "component": MembershipSetupComponent
  },

  /* Lazy Loading  */
  { path: '', loadChildren: './child/child.module#ChildModule' },

  { path: 'login', component: LoginComponent },

  { path: 'admin',loadChildren: './admin/admin.module#AdminModule'},
  
  { path: '**', component: ErrorPageComponent },
  
]

export const routing = RouterModule.forRoot(routes);
